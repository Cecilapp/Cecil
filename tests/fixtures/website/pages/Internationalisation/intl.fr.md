---
title: Internationalisation (FR)
date: 31-07-2019
updated: 2022-09-07
layout: intl
menu: main
---
<!-- break -->
- Utiliser `{% trans %}Texte{% endtrans %}` pour traduire du texte
- Utiliser le filtre `format_date` pour localiser une date
